use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct DeletionMessage {
    pub metadata: Metadata,
    pub notification: Notification,
}
#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Metadata {
    pub topic: String,
    pub schema_version: String,
    pub deprecated: bool,
}
#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Notification {
    pub notification_id: String,
    pub event_date: String,
    pub publish_date: String,
    pub publish_attempt_count: i32,
    pub data: NotificationData,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct NotificationData {
    pub username: String,
    pub user_id: String,
    pub eias_token: String,
}
