pub mod challenge;
pub mod deletion;
pub mod ebay;
pub mod test;

#[test]
fn test_ebay_identiy_deser() {
    let json_str = r#"{
        "userId": "yY5CV-HLQgG",
        "username": "keithmarsh2",
        "accountType": "BUSINESS",
        "registrationMarketplaceId": "EBAY_GB",
        "businessAccount": {
            "name": "Viztech",
            "email": "keith@viztech.co.uk",
            "primaryPhone": {
                "countryCode": "GB",
                "number": "7765401810",
                "phoneType": "MOBILE"
            },
            "address": {
                "addressLine1": "11 Green Lane",
                "addressLine2": "Farncombe",
                "city": "Godalming",
                "stateOrProvince": "Surrey",
                "postalCode": "GU7 3SN",
                "country": "GB"
            },
            "primaryContact": {
                "firstName": "Keith",
                "lastName": "Marsh"
            }
        }
    }"#;
    let _identity: ebay::EbayIdentity = serde_json::from_str(json_str).unwrap();
}
