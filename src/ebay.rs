use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct OAuthParams {
    pub code: String,
    pub expires_in: usize,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct OAuthTokens {
    pub access_token: String,
    pub expires_in: u32,
    pub refresh_token: String,
    pub refresh_token_expires_in: u64,
    pub token_type: String,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct EbayPhone {
    pub country_code: String,
    pub number: String,
    pub phone_type: String
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct EbayAddress {
    pub address_line_1: String,
    pub address_line_2: String,
    pub city: String,
    pub state_or_province: String,
    pub postal_code: String,
    pub country: String
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct EbayName {
    pub first_name: String,
    pub last_name: String
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub enum EbayAccount {
    BusinessAccount {
        name: String,
        email: String,
        #[serde(rename = "primaryPhone")]
        primary_phone: EbayPhone,
        address: EbayAddress,
        #[serde(rename = "primaryContact")]
        primary_contact: EbayName,
    },
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct EbayIdentity {
    pub user_id: String,
    pub username: String,
    pub account_type: String,
    pub registration_marketplace_id: String,
    #[serde(flatten)]
    pub account: EbayAccount,
}
