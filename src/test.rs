use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct TestMessage {
    pub verb: String,
    pub message: String,
}
